<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddKeyTokens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('oauth_clients')->insert([
            'id'                     => 1,
            'name'                   => 'Pasaporte Chaco Personal Access Client',
            'secret'                 => '8GU3RW5VxoqjfN48vvdDwtVuEgZT4QxaZvQT3ppQ',
            'redirect'               => 'http://localhost',
            'personal_access_client' => 1,
            'password_client'        => 0,
            'revoked'                => 0
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
