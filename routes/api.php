<?php

use App\Http\Controllers\AutoevaluationController;
use App\Http\Controllers\ExceptionDnuController;
use App\Http\Controllers\LocationsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TgdController;
use App\Http\Controllers\PasaporteController;
use App\Http\Controllers\PeopleController;
use App\Http\Controllers\SaludController;
use App\Http\Controllers\TurnoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/find-user', [TgdController::class,'findUserWithCuit']);
Route::post('/find-user-from-callback-of-tgd', [TgdController::class,'findUserFromCallbackOfTgd']);
Route::get('token/{code*}', [TgdController::class, 'getTgdToken']);
// ['auth:api']
Route::middleware([])->group(function() {
    //code
    Route::post('/send-code', [TgdController::class, 'sendCode']);
    Route::post('/validate-code', [TgdController::class, 'validateCode']);
    Route::get('/accessCode/{cuil}', [TgdController::class, 'getCode']);

    //pasaporte
    Route::post('/pasaporte/store',[PasaporteController::class,'store']);
    Route::get('/pasaporte/estado/{idPersona}', [PasaporteController::class,'estado']);
    Route::get('/pasaporte/certificado/{idPersona}', [PasaporteController::class,'certificado']);
    Route::get('/pasaporte/getHisopados/{idPersona}', [PasaporteController::class,'getHisopados']);
    Route::get('/pasaporte/{idPersona}',[PasaporteController::class,'show']);

    // locations
    Route::get('/provincies', [LocationsController::class,'getProvincies']);
    Route::get('/deparments/{idProvince}', [LocationsController::class,'getDepartments']);
    Route::get('/locations/{idDeparmet}', [LocationsController::class,'getLocations']);
    Route::get('/locations-destination/{idProvince?}', [LocationsController::class,'getLocationsDestination']);
    Route::get('/list-motivos/{type?}', [LocationsController::class,'getMotivos']);

    // Autoevaulacion
    Route::post('/autoevaluacion', [AutoevaluationController::class, 'store']);

    //Personas
    Route::post('/personas', [PeopleController::class, 'store']);
    Route::get('/personas/{idPersona}', [PeopleController::class, 'show']);
    Route::put('/personas/{idPersona}', [PeopleController::class, 'update']);
    Route::put('/acceptTerms/{idPersona}', [PeopleController::class, 'updateTerms']);

    //Exception
    Route::get('/exceptions', [ExceptionDnuController::class, 'index']);

    //Turno
    Route::get('/turno/{dni}/{sex}',[TurnoController::class, 'findTurno']);
    Route::get('/vacunas/{dni}/{sex}',[TurnoController::class, 'getVacunas']);

    //Terminos
    Route::get('/terms', [PeopleController::class, 'getTerms']);


    // Informar Contacto Estrecho
    Route::post('/info-contacto-estrecho',[SaludController::class, 'infoContactoEstrecho']);
});

// Route::get('fix', [TgdController::class,'fixTgdId']);
