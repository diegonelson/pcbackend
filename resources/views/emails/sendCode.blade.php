@component('mail::message')
# Hola {{ ucwords(strtolower($user->name)) }},

 
Ingrese el código en la aplicación de Pasaporte Chaco para validar su usuario.

@component('mail::panel')
Código de Verificación: {{ $user->code }}
@endcomponent

{{-- 
@component('mail::button', ['url' => ''])
Button Text
@endcomponent --}}

Gracias,<br>
{{ config('app.name') }}
@endcomponent
