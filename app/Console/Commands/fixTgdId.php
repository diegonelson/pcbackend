<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class fixTgdId extends Command
{
  private $token;
  private $urlBaseTgd;
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'tgd:fix-cuil';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Command description';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
    $this->urlBaseTgd = config('services.tgd.url_base');
    // $this->info('Pidiendo token');
    $this->token = $this->getTgdToken();
    // $this->info('Pidiendo token: done');
  }

  /**
   * Execute the console command.
   *
   * @return int
   */
  public function handle()
  {
    $this->info('Pidiendo personas');
    $urlBase = config('services.permisos.url_base');
    $personas = json_decode(Http::get($urlBase . '/people-without-tgd-id')->body());
    $this->info('Hay personas: '.count($personas));
    // dd($urlBase);
    foreach ($personas as $persona) {
      // dd($persona);
      if ($persona->cuil) {
        // $this->info('pidiendo tgd');
        $user = json_decode(
          Http::withToken($this->token)
          ->get($this->urlBaseTgd . '/api/v1/persona/datos-por-cuil/​' . $persona->cuil)
          ->body()
        );
        // dd($user);
        if (!isset($user->code)) {
          $this->info('actualizando persona');
          $res = Http::put($urlBase . '/update-tgd-id/' . $persona->id, ['tgd_id' => $user->id]);
          $this->info('cuil: '.$persona->cuil.' - tgd: '. $user->id);
        }else{
          $this->info('cuil: '.$persona->cuil.' - error: '. $user->message);
        }
      }
      // $user = $this->findUserWithCuit($persona->cuit);  // User::where('cuitCuil',$persona->cuil)->get();
    }
  }

  private function getTgdToken()
  {
    $response = Http::get($this->urlBaseTgd . '/oauth/v2/token', [
      'grant_type'    => 'client_credentials',
      'client_id'     => config('services.tgd.client_id'),
      'client_secret' => config('services.tgd.client_screcet')
    ]);
      // dd($response);
    $token = json_decode($response->body())->access_token;
    
    return $token;
  }
}
