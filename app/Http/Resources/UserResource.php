<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'tgdId'        => $this->tgdId,
            'cuitCuil'     => $this->cuitCuil,
            'name'         => ucwords(strtolower($this->name)),
            'lastName'     => ucwords(strtolower($this->lastName)),
            'email'        => $this->email,
            'phone'        => $this->phone,
            'phoneCompany' => $this->phoneCompany,
            'level'        => $this->level,
            'levelName'    => $this->levelName,
            'code'         => $this->code,
            'status'       => $this->status,
            'sex'          => $this->sex ? $this->sex : 'A',
            'dni'          => $this->dni
        ];
    }
}
