<?php

namespace App\Http\Controllers;

use Http;
use Illuminate\Http\Request;

class LocationsController extends Controller
{
    public function getProvincies()
    {
        $response = Http::get(config('services.permisos.url_base').'/provincias');

        return json_decode($response->body())->data;
    }

    public function getDepartments($idProvince)
    {
        $response = Http::get(config('services.permisos.url_base').'/departamentos-por-provincia/' . $idProvince);
        return json_decode($response->body())->data;
    }

    public function getLocations($idDeparmet)
    {
        $response = Http::get(config('services.permisos.url_base').'/localidades-por-departamento/' . $idDeparmet);
        return json_decode($response->body())->data;
    }

    public function getLocationsDestination($idProvince = 22)
    {
        $response = Http::get(config('services.permisos_provincia.url_base').'/provinces/' . $idProvince . '/departments/cities');
        return json_decode($response->body());
    }

    public function getMotivos($type = 'T')
    {
        $response = Http::get(config('services.permisos_provincia.url_base').'/motivos.json?tipo='.$type);
        return json_decode($response->body());
    }
}
