<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use App\Mail\SendCode;
use App\Models\Sms;
use Carbon\Carbon;
use Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;

class TgdController extends Controller
{

  private $urlBaseTgd;
  private $token;

  public function __construct()
  {
    $this->urlBaseTgd = config('services.tgd.url_base');
    // $this->token = $this->getTgdToken();
  }

  public function findUserWithCuit(Request $request)
  {
    $this->token =  $this->getTgdToken($request->code);

    try {
      $user = $this->getUserFromTgd($request->cuit);

      if (!$user)  return response()->json(['status' => false]);
      if ($user == 'nivel0') return $this->userNivel0();

      $urlBase = config('services.permisos.url_base');
      $personaId = json_decode(Http::get($urlBase . '/personaFromTgdId/' . $user->tgdId)->body())->idPersona;
      
      $token = $user->createToken($user->cuitCuil)->accessToken;

      return response()->json([
        'status' => true,
        'token'  => $token,
        'personaId' => $personaId,
        'user'   => new UserResource($user),
      ]);
    } catch (\Throwable $th) {
      return response([
        'status' => false,
        'error' => $th->getMessage()
      ], 500);
    }
  }

  public function findUserFromCallbackOfTgd(Request $request)
  {
    $this->token =  $this->getTgdToken($request->code);

    try {
      $user = $this->getUserFromTgd();
      // dd($user);
      if (!$user)  return response()->json(['status' => false]);
      if ($user == 'nivel0') return $this->userNivel0();

      $urlBase = config('services.permisos.url_base');
      $personaId = json_decode(Http::get($urlBase . '/personaFromTgdId/' . $user->tgdId)->body())->idPersona;
      
      $token = $user->createToken($user->cuitCuil)->accessToken;
      
      return response()->json([
        'status' => true,
        'token'  => $token,
        'personaId' => $personaId,
        'user'   => new UserResource($user),
        // 'url'    => config('sanctum.stateful')
      ]);
    } catch (\Throwable $th) {
      return response([
        'status' => false,
        'error' => $th->getMessage()
      ], 500);
    }
  }

  public function sendCode(Request $request)
  {
    $user  = User::find($request->id);

    $code = rand(111111, 999999);
    $user->update(['code' => $code]);

    if ($request->method == 'mail') Mail::to($user)->send(new SendCode($user));
    if ($request->method == 'sms') $this->chekCompanyAndSendSms($user);

    return response()->json(['status' => true]);
  }

  public function validateCode(Request $request)
  {
    $user = User::find($request->id);
    if ($user->code == $request->code) return response()->json(['status' => true]);
    return response()->json(['status' => false]);
  }

  public function getCode($cuil)
  {
    $user = User::where('cuitCuil', $cuil)->first();

    if (!$user) return response(['status' => false]);

    return response([
      'status'     => true,
      'cuil'       => $user->cuitCuil,
      'accsesCode' => $user->code,
      'fullName'   => $user->lastName . ', ' . $user->name,
      'tgdId'      => $user->tgdId
    ]);
  }

  /// PRIVATES METHODS /////////////////////////////////////////////////////////////
  private function getUserFromTgd($cuit = null)
  {
    if (!$this->token) return null;

    $findByCuil = $cuit == null ? null : '/datos-por-cuil/​';

    $userFromTgd = json_decode(
      Http::withToken($this->token)
        ->get($this->urlBaseTgd . '/api/v1/persona' . $findByCuil . $cuit)
        ->body()
    );

    // dd($userFromTgd);
    if (isset($userFromTgd->code)) {
      if($userFromTgd->message == 'La persona tiene nivel de acreditación pendiente.') return 'nivel0';
      return false;
    };
    
    try {
      $user = $this->storeUser($userFromTgd);
    } catch (\Throwable $th) {
      throw $th;
    }

    return $user;
  }

  public function getTgdToken($code = null)
  {
    // dd($code);
    $query = 'grant_type=' . ($code != null ? 'authorization_code' : 'client_credentials')
      . '&client_id=' . config('services.tgd.client_id')
      . '&client_secret=' . config('services.tgd.client_screcet');


    $uri = $code == null ? '' : '&redirect_uri=' . urlencode(config('services.pasaporte_landing.url_base') . '/callback-tgd') . '&code=' . $code;

    $response = Http::get($this->urlBaseTgd . '/oauth/v2/token?' . $query . $uri);

    if (isset(json_decode($response->body())->error)) return null;
    $token = json_decode($response->body())->access_token;

    return $token;
  }

  private function storeUser($userFromTgd)
  {
    $user = User::updateOrCreate(
      [
        'tgdId'     => $userFromTgd->id,
      ],
      [
        'name'         => $userFromTgd->nombres,
        'lastName'     => $userFromTgd->apellidos,
        'email'        => $userFromTgd->emails[0]->email,
        'cuitCuil'     => $userFromTgd->cuitCuil,
        'level'        => $userFromTgd->nivel,
        'levelName'    => $userFromTgd->nivelStr,
        'status'       => 'noApproved',
        'phone'        => isset($userFromTgd->telefonos) ? $this->getPhoneFromTgd($userFromTgd->telefonos) : null,
        'phoneCompany' => isset($userFromTgd->telefonos) ? $this->getPhoneCompanyFromTgd($userFromTgd->telefonos) : null,
        'dni'          => count($userFromTgd->tiposDocumentoPersona) ? $userFromTgd->tiposDocumentoPersona[0]->numeroDocumento : null,
        'sex'          => $userFromTgd->sexo == null ? 'A' : $userFromTgd->sexo,
      ]
    );

    return $user;
  }

  private function formatNumber($number)
  {
    $explodeNumber = explode(' ', $number);
    if (count($explodeNumber) <= 1) return $number;
    return ltrim($explodeNumber[0], 0) . str_replace('-', '', str_replace('15-', '', $explodeNumber[1]));
  }

  private function chekCompanyAndSendSms(User $user)
  {
    if ($user->phoneCompany) {

      if ($user->phoneCompany == 'Tuenti') {
        $company = 'Movistar';
      } else $company = $user->phoneCompany;

      $this->sendSms($user,  $this->services[$company]);
    } else {
      foreach ($this->services as $service) {
        $this->sendSms($user, $service);
      }
    }
  }

  private function sendSms(User $user, $servicio)
  {
    $number = '54' . $user->phone;
    $code = $user->code;
    $message = 'Su codigo de verificacion para Pasaporte Chaco es: ' . $code . '. No comparta este codigo con nadie.';
    $url = 'http://smsserver.ecomchaco.com.ar/mt.php?servicio=' . $servicio . '&msisdn=' . $number . '&contenido=' . urlencode($message);
    Http::get($url);

    $date = Carbon::now()->format('Y-m-d');

    Sms::firstOrCreate(['date' => $date])
      ->increment('sentMessages');
  }

  private $services = [
    'Personal' => '22210.personal.ar',
    'Claro' => '22210.cti.ar',
    'Movistar' => '22210.movistar.ar',
    // 'Nextel' => '22210.nextel.ar'
  ];

  private function getPhoneFromTgd($phones)
  {
    if (count($phones) == 0) return null;

    return  isset($phones[0]->telefonoPlainText) && $phones[0]->telefonoPlainText != null
      ? $this->formatNumber($phones[0]->telefonoPlainText)
      : null;
  }

  private function getPhoneCompanyFromTgd($phones)
  {
    if (count($phones) == 0) return null;

    // return isset($phones[0]->empresaCelular->nombre) && $phones[0]->empresaCelular->nombre != null 
    return isset($phones[0]->empresaCelular->nombre) &&   $phones[0]->empresaCelular->nombre != null
      ? $phones[0]->empresaCelular->nombre
      : null;
  }

  public function fixTgdId()
  {
    $urlBase = config('services.permisos.url_base');
    $personas = json_decode(Http::get($urlBase . '/people-without-tgd-id')->body());
    // dd($urlBase);
    foreach ($personas as $persona) {
      // dd($persona);
      if ($persona->cuil) {
        $user = json_decode(
          Http::withToken($this->token)
            ->get($this->urlBaseTgd . '/api/v1/persona/datos-por-cuil/​' . $persona->cuil)
            ->body()
        );
        // dd($user);
        if (!isset($user->code)) {
          Http::put($urlBase . '/update-tgd-id/' . $persona->id, ['tgd_id' => $user->id]);
        }
        // dd($res->body());
      }
      // $user = $this->findUserWithCuit($persona->cuit);  // User::where('cuitCuil',$persona->cuil)->get();
    }
    return $personas;
  }

  private function userNivel0()
  {
    return response()->json([
      'status' => true,
      'user'   => [
        'level' => '0'
      ],
    ]);
  }
}
