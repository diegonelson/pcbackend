<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\SendCode;
use Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;

class TurnoController extends Controller
{

    private $urlBase;

    public function __construct()
    {
        $this->urlBase = config('services.vacunacion.url_base');
    }



    public function findTurno($dni, $sex){
        $response = Http::get($this->urlBase.'/turnos/dni/'.$dni.'/sexo/'.$sex)->json();

        return response()->json([
            'status' => true,
            'data' => $response
        ]);
    }

    public function getVacunas($dni, $sex){

        $response = Http::get($this->urlBase.'/vacunaciones/dni/'.$dni.'/sexo/'.$sex)->json();

        return response()->json([
            'status' => true,
            'vacunas' => $response
        ]);
    }


}


