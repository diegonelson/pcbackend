<?php

namespace App\Http\Controllers;

use Http;
use Illuminate\Http\Request;

class ExceptionDnuController extends Controller
{
    private $urlBase;

    public function __construct()
    {
        $this->urlBase = config('services.permisos.url_base');
    }

    public function index()
    {
        $response = Http::get($this->urlBase . '/excepciones-dnu');
        return json_decode($response->body());
    }
}
