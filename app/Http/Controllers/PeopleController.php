<?php

namespace App\Http\Controllers;

use Http;
use Illuminate\Http\Request;

class PeopleController extends Controller
{
  private $urlBase;
  private $urlBaseTgd;
  private $token;


  public function __construct()
  {
    $this->urlBase = config('services.permisos.url_base');
    $this->urlBaseTgd = config('services.tgd.url_base');
    $this->token = $this->getTgdToken();
  }

  public function store(Request $request)
  {

    if (!isset($request->tgd_id)) {
      $tgdId = $this->getUserFromTgd($request->cuil);
    } else {
      $tgdId = $request->tgd_id;
    };

    // return $this->urlBase;
    if ($tgdId) {

      $response = Http::post($this->urlBase . '/personas', [
        'dni'            => $request->dni,
        'nombre'         => $request->nombre,
        'apellido'       => $request->apellido,
        'email'          => $request->email,
        'telefono'       => $request->telefono,
        'sexo'           => $request->sexo,
        'cuil'           => $request->cuil,
        'tgd_id'         => $tgdId,
        'city_id'        => $request->city_id,
        'direccion'      => $request->direccion,
        'nummero'        => $request->direccion_nro,
        'barrio'         => $request->barrio,
        'phoneCompany'   => $request->phoneCompany,
        'dateAcceptTerm' => $request->dateAcceptTerm
      ]);

      // dd($response->body());
    } else {
      return response([
        'status' => false,
        'message' => 'No se pudo obtener el usuario de Tu Gobierno Digital'
      ]);
    }

    return response()->json([
      'status' => true,
      'persona' => $response->json()
    ]);
  }


  public function show($idPersona)
  {
    $response = Http::get($this->urlBase . '/personas/' . $idPersona);
    return response()->json([
      'status' => true,
      'pasaporte' => $response->json()
    ]);
  }

  public function update(Request $request, $idPersona)
  {
    if (!isset($request->tgd_id)) {
      $tgdId = $this->getUserFromTgd($request->cuil);
    } else {
      $tgdId = $request->tgd_id;
    };

    if ($tgdId) {
      $response = Http::put($this->urlBase . '/personas/' . $idPersona, [
        'telefono'       => $request->telefono,
        'email'          => $request->email,
        'direccion'      => $request->direccion,
        'numero'         => $request->direccion_nro,
        'barrio'         => $request->barrio,
        'phoneCompany'   => $request->phoneCompany,
        'sexo'           => $request->sexo,
        'tgd_id'         => $tgdId,
        'dateAcceptTerm' => $request->dateAcceptTerm
      ]);
    } else {
      return response([
        'status' => false,
        'message' => 'No se pudo obtener el usuario de Tu Gobierno Digital'
      ]);
    }

    return response()->json([
      'status' => true,
      'pasaporte' => $response->json()
    ]);
  }

  public function updateTerms(Request $request, $idPersona)
  {
    $response = Http::put($this->urlBase . '/acceptTerms/' . $idPersona, [
      'dateAcceptTerm' => $request->dateAcceptTerm
    ]);

    $persona = $response->json();
    return $persona;
    return response([
      'status' => true,
      'dateAcceptTerm' => $persona->dateAcceptTerm
    ]);
  }

  public function getTerms()
  {
    $response = Http::get(config('services.permisos_provincia.url_base') . '/base_condicion');

    $terms = json_decode($response->body());

    return response()->json([
      'terms' => $terms->description,
      'validSince' => $terms->validSince
    ]);
  }

  private function getUserFromTgd($cuit)
  {

    if (!$this->token) return null;

    $userFromTgd = json_decode(
      Http::withToken($this->token)
        ->get($this->urlBaseTgd . '/api/v1/persona/datos-por-cuil/​' . $cuit)
        ->body()
    );

    if (isset($userFromTgd->code)) return null;

    return $userFromTgd->id;
  }

  private function getTgdToken()
  {
    $response = Http::get($this->urlBaseTgd . '/oauth/v2/token', [
      'grant_type'    => 'client_credentials',
      'client_id'     => config('services.tgd.client_id'),
      'client_secret' => config('services.tgd.client_screcet')
    ]);

    $token = json_decode($response->body())->access_token;

    return $token;
  }
}
