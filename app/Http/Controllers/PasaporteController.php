<?php

namespace App\Http\Controllers;

use App\Http\Requests\PasaporteStoreRequest;
use Http;
use Illuminate\Http\Request;

class PasaporteController extends Controller
{
    private $urlBase;

    public function __construct()
    {
        $this->urlBase = config('services.permisos.url_base');
    }

    public function store(PasaporteStoreRequest $request)
    {
        $response = Http::post($this->urlBase . '/solicitar-pasaporte', [
            'tipo_transporte'  => $request->tipoTransporte,
            'fecha_arribo'     => $request->fechaIngreso,
            'fecha_salida'     => $request->fechaEgreso,
            'persona_id'       => $request->idPersona,
            'patentes'         => $request->patentes,
            'excepciones'      => $request->excepciones,
            'acompanantes'     => $request->acompanantes,
            'tipo_empleado'    => $request->tipoEmpleado,
            'motivo_id'        => $request->motivo_id,
            'localidadDestino' => $request->localidadDestino,
        ]);

        return $response->json();
    }

    public function show($idPersona)
    {
        $response = json_decode(Http::get($this->urlBase . '/pasaportes/' . $idPersona)->body(), true);

        if (!$response['status']) return response()->json(['status' => false]);

        return response()->json([
            'status' => true,
            'pasaporte' => $response['pasaporte']
        ]);
    }

    public function estado($idPersona)
    {
        $cuil = $this->getPeople($idPersona);
        
        if (!$cuil) return response()->json(['status' => false]);

        $response = Http::get(config('services.permisos_provincia.url_base').'/estado/' . $cuil->cuilCuit);
        
        return response()->json([
            'status' => true,
            'estado' => json_decode($response->body(), true)
        ]);
    }

    public function certificado($idPersona)
    {
        $people = $this->getPeople($idPersona);
        
        if (!$people) return response()->json(['status' => false]);
        if (!$people) return response(['status' => false]);

        $response = Http::get(config('services.permisos_provincia.url_base').'/pasaporte/qr/' . $people->cuilCuit);
        
        return response()->json([
            'status'      => true,
            'certificado' => json_decode($response->body(), true)
        ]);
    }

    public function getHisopados($idPersona)
    {
        $people = $this->getPeople($idPersona);
        
        if (!$people) return response(['status' => false]);

        $url = config('services.permisos_provincia.url_base').'/registrosalud/hisopados/' . $people->dni . '/' . $people->sexo;
        
        $response = Http::get($url);
        
        return response()->json([
            'status'     => true,
            'lastTested' => json_decode($response->body(), true)
        ]);
    }

    private function getPeople($id)
    {
        $body = json_decode(Http::get($this->urlBase . '/personas/' . $id)->body());
        
        if (!$body->status) return false;
        
        return $body->persona;
    }
}
