<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Http;

class SaludController extends Controller
{
  private $urlBase;

  public function __construct()
  {
    $this->urlBase = config('services.salud_url');
  }
  public function infoContactoEstrecho(Request $request)
  {
    try {
      $response = Http::post($this->urlBase, [
        'RegistroCasoDNI'              => $request->RegistroCasoDNI,
        'registrocasosexo'             => $request->registrocasosexo,
        'RegistroCasoSintomaPerNombre' => $request->RegistroCasoSintomaPerNombre,
        'RegistroCasoSintomaFecha'     => $request->RegistroCasoSintomaFecha,
        'pMunicipioNombre'             => $request->pMunicipioNombre,
        'pRegistroCasoDireccion'       => $request->pRegistroCasoDireccion,
        'pRegistroCasoEmail'           => $request->pRegistroCasoEmail,
        'pRegistroCasoTelefono'        => $request->pRegistroCasoTelefono,
        'RegistroCasoFiebre'           => $request->RegistroCasoFiebre,
        'RegistroCasoDificultadResp'   => $request->RegistroCasoDificultadResp,
        'RegistroCasoTos'              => $request->RegistroCasoTos,
        'RegistroCasoDolorGarganta'    => $request->RegistroCasoDolorGarganta,
        'RegistroCasoDiarrea'          => $request->RegistroCasoDiarrea,
        'RegistroCasoFRPatoBase'       => $request->RegistroCasoFRPatoBase,
        'RegistroCasoSintomaOtro'      => $request->RegistroCasoSintomaOtro
      ]);

      $response->throw();
  
      return response([
        'status' => true
      ]);
      
    } catch (\Exception $exception) {
      return response([
        'status' => false,
        'message' => $exception->getMessage()
      ], $response->status());
    }
  }
}
