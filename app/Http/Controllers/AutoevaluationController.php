<?php

namespace App\Http\Controllers;

use Http;
use Illuminate\Http\Request;

class AutoevaluationController extends Controller
{

    private $urlBase;

    public function __construct(){
        $this->urlBase = config('services.permisos.url_base');
    }

    public function store(Request $request){
        
        $response = Http::post($this->urlBase.'/autoevaluacion',[
            'contactoEstrecho'      => $request->contactoEstrecho,
            'contactoViajero'       => $request->contactoViajero,
            'fiebre'                => $request->fiebre,
            'faltaAire'             => $request->faltaAire,
            'tos'                   => $request->tos,
            'dolorGarganta'         => $request->dolorGarganta,
            'diarrea'               => $request->diarrea,
            'enfermedadCronica'     => $request->enfermedadCronica,
            'alteracionGustoOlfato' => $request->alteracionGustoOlfato,
            'persona_id'            => $request->persona_id
        ]);
        
        return response()->json($response->body(), 200);
    }
}
