<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PasaporteStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipoEmpleado'   => 'required|numeric',
            'idPersona'      => 'required|numeric',
            'fechaIngreso'   => 'date|nullable',
            'fechaEgreso'    => 'date|nullable',
            'tipoTransporte' => 'string|nullable',
            'patentes'       => 'array|nullable',
            'acompanantes'   => 'array|nullable',
            'excepciones'    => 'string|nullable'
        ];
    }
}
