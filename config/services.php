<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain'   => env('MAILGUN_DOMAIN'),
        'secret'   => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key'    => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'tgd' => [
        'client_id'      => env('TGD_CLIENT_ID'),
        'client_screcet' => env('TGD_CLIENT_SECRET'),
        'url_base'       => env('TGD_URL_BASE')
    ],
    'permisos' => [
        'url_base' => env('PERMISOS_URL_BASE')
    ],
    'vacunacion' => [
        'url_base' => env('VACUNACION_URL_BASE')
    ],
    'permisos_provincia' => [
        'url_base' => env('PERMISOS_PROVINCIA_URL_BASE')
    ],
    'pasaporte_landing' => [
        'url_base' => env('URL_PASAPORTE_LANDING')
    ],
    'salud_url' => env('URL_CONTACTO_ESTRECHO')

];
